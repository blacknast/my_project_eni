drop table Agendas;
drop table Personnels;
drop table Races;
drop table Animaux;
drop table Clients;


go
create table Personnels(
CodePers bigint not null identity (10000, 100) primary key,
Nom varchar (30),
MotPasse varchar (10),
R�le varchar (3),
Archive bit,
constraint Personnels_r�le_ck check(R�le in ('sec', 'vet', 'adm'))
);


go
create table Clients(
CodeClient bigint not null identity (1, 1) primary key,
Client varchar(20),
PrenomClient varchar(20),
Adresse1 varchar(50),
Adresse2 varchar(30),
CodePostal varchar (6),
Ville varchar (25),
NumTel varchar (15),
Assurance varchar (30),
Email varchar (50),
Remarque varchar,
Archive bit
);

go
create table Animaux(
CodeAnimal bigint not null identity (100, 10) primary key,
NomAnimal varchar (30),
Sexe char (1) null,
Couleur varchar (20),
Race varchar (20),
Espece varchar (20),
CodeClient bigint not null,
Tatouage varchar (10),
Antecedents varchar,
Archive bit,
constraint fk_CodeClient foreign key (CodeClient) references Clients(CodeClient),
constraint Animaux_sexe_ck check(sexe in ('M', 'F', 'H'))
);

go
create table Agendas(
CodeVeto bigint not null identity,
DateHeureRdv datetime check(DateHeureRdv in('00', '15', '30', '45')),
CodeAnimal bigint not null,
Primary key (CodeVeto, DateHeureRdv, CodeAnimal),
constraint fk_CodeAnimal foreign key (CodeAnimal) references Animaux(CodeAnimal)
);


go
create table Races(
Race varchar (20),
Espece varchar (20),
constraint pk primary key (Race, Espece)
);



select * from Clients;
select * from Animaux;
select * from Agendas;
select * from Personnels;
select * from Races;


