/* Rajouter des donn�es */



set IDENTITY_insert Personnels ON

insert into Personnels (Nom, MotPasse, Role) values ('Bosapin', 'edmont', 'vet');
insert into Personnels (Nom, MotPasse, Role) values ('Cajou', 'benoit', 'adm');
insert into Personnels (Nom, MotPasse, Role) values ('Malalanich', 'v�tomelani', 'vet');
insert into Personnels (Nom, MotPasse, Role) values ('Aimone', 'anneveto', 'vet');
insert into Personnels (Nom, MotPasse, Role) values ('Tourne', 'sylvanveto', 'vet');
insert into Personnels (Nom, MotPasse, Role) values ('De Jeu', 'odettesec', 'sec');
insert into Personnels (Nom, MotPasse, Role) values ('Abondieu', 'elisabeth', 'sec');
insert into Personnels (Nom, MotPasse, Role) values ('Hisson', 'mariepaul', 'sec');


insert into Races (Race, Espece) values ('Angora', 'Chat');
insert into Races values ('Sacr� de Birmanie', 'Chat');
insert into Races values ('Siamois', 'Chat');
insert into Races values ('Berger Allemand', 'Chien');
insert into Races values ('Caniche', 'Chien');
insert into Races values ('Colais', 'Chien');
insert into Races values ('Dobberman', 'Chien');
insert into Races values ('Labrador', 'Chien');
insert into Races values ('Pitbull', 'Chien');
insert into Races values ('Rotweiller', 'Chien');




/*
insert into Agendas values ('11-09-2017 08:00:00:000', '');
insert into Agendas values ('11-09-2017', '08:15:00:000', 1);
insert into Agendas values ('11-09-2017', '09:30:00:000', 2);
insert into Agendas values ('11-09-2017', '09:45:00:000', 2);
insert into Agendas values ('11-09-2017', '10:00:00:000', 5);
insert into Agendas values ('11-09-2017', '10:15:00:000', 5);
insert into Agendas values ('11-09-2017', '11:45:00:000', 9);
insert into Agendas values ('11-09-2017', '12:00:00:000', 9);
insert into Agendas values ('11-09-2017', '14:45:00:000', 7);
insert into Agendas values ('11-09-2017', '15:00:00:000', 7);

insert into Agendas values ('12-09-2017', '09:00:00:000', 3);
insert into Agendas values ('12-09-2017', '09:15:00:000', 3);
insert into Agendas values ('12-09-2017', '09:30:00:000', 4);
insert into Agendas values ('12-09-2017', '09:45:00:000', 4);
insert into Agendas values ('12-09-2017', '10:30:00:000', 6);
insert into Agendas values ('12-09-2017', '15:15:00:000', 8);
insert into Agendas values ('12-09-2017', '15:30:00:000', 8);


insert into Agendas values ('13/09/2017', '08:30:00:000', 10);
insert into Agendas values ('13/09/2017', '08:45:00:000', 10);
insert into Agendas values ('13/09/2017', '09:30:00:000', 13);
insert into Agendas values ('13/09/2017', '09:45:00:000', 13);
insert into Agendas values ('13/09/2017', '11:30:00:000', 19);
insert into Agendas values ('13/09/2017', '16:15:00:000', 15);
insert into Agendas values ('13/09/2017', '16:30:00:000', 15);
*/

set IDENTITY_insert Clients ON
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Dubosc', 'Frank', '12 avenue des flots bleus', 'non renseign�', '76140', 'Petit-Quevilly', '06xxxxxxxx', 'Bobo des �les', 'dubosc.frank@vetopupuce.com', 'pas l''temps d''niaiser');
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Boon', 'Dany', '22 rue des Ch''tis', 'non renseign�','59280', 'Armenti�res', '06xxxxxxxx', 'Bobo des �les', 'boon.dany@vetopupuce.com', 'pas l''temps d''niaiser');
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Elmaleh', 'Gad', '45 rue du sentier', 'non renseign�','75001', 'Paris', '06xxxxxxxx', 'Mafassur', 'elmaleh.gad@vetopupuce.com', 'pas l''temps d''niaiser');
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Dujardin', 'Jean', 'rue Brice', 'non renseign�','92500', 'Rueil-Malmaison', '06xxxxxxxx', 'Bobo des �les', 'dujardin.jean@vetopupuce.com', 'pas l''temps d''niaiser');
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Marceau', 'Sophie', 'Boulevard de la Boom', 'non renseign�','75010', 'Paris', '06xxxxxxxx', 'Bobo des �les', 'marceau.sophie@vetopupuce.com', 'pas l''temps d''niaiser');
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Merad', 'Kad', 'Rue du petit nicolas', 'non renseign�','91130', 'Ris-Orangis', '06xxxxxxxx', 'Mafassur', 'merad.kad@vetopupuce.com', 'pas l''temps d''niaiser');
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Seigner', 'Mathilde', '357 rue du Camping', 'non renseign�','75012', 'Paris', '06xxxxxxxx', 'Bobo des �les', 'seigner.mathilde@vetopupuce.com', 'pas l''temps d''niaiser');
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Reno', 'Jean', '78 Boulevard de L�on', 'non renseign�','51200', 'Montmirail', '06xxxxxxxx', 'Bobo des �les', 'reno.jean@vetopupuce.com', 'pas l''temps d''niaiser');
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Lanvin', 'G�rard', '84 avenue de l''aile ou la cuisse', 'non renseign�','92100', 'Boulogne-Billancourt', '06xxxxxxxx', 'Bobo des �les', 'lanvin.gerard@vetopupuce.com', 'pas l''temps d''niaiser');
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Tautou', 'Audrey', 'rue de Montmartre', 'non renseign�','63110', 'Beaumont', '06xxxxxxxx', 'Mafassur', 'tautou.audrey@vetopupuce.com', 'pas l''temps d''niaiser');
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Cotillard', 'Marion', '45 rue de la M�me', 'non renseign�','13001', 'Marseille', '06xxxxxxxx', 'Mafassur', 'cotillard.marion@vetopupuce.com', 'pas l''temps d''niaiser');
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Duris', 'Romain', '76 rue de l''arnaqueur', 'non renseign�','06000', 'Nice', '06xxxxxxxx', 'Mafassur', 'duris.romain@vetopupuce.com', 'pas l''temps d''niaiser');
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Depardieu', 'G�rard', '57 rue du conte de Mont�-Cristo', 'non renseign�', '36000', 'Ch�teauroux', '06xxxxxxxx', 'Bobo des �les', 'depardieu.gerard@vetopupuce.com', 'pas l''temps d''niaiser');
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Youn', 'Micha�l', 'rue de la beuze', 'non renseign�', '92150', 'Suresnes', '06xxxxxxxx', 'Bobo des �les', 'youn.mickael@vetopupuce.com', 'pas l''temps d''niaiser');
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Poelvoorde', 'Beno�t', '22 rue du Boulet', 'non renseign�', '22500', 'Paimpol', '06xxxxxxxx', 'Mafassur', 'poelvoorde.benoit@vetopupuce.com', 'pas l''temps d''niaiser');
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Paradis', 'Vanessa', '12 rue des arnaqueurs', 'non renseign�', '94100', 'Saint-Maur-des-Foss�s', '06xxxxxxxx', 'Bobo des �les', 'paradis.vanessa@vetopupuce.com', 'pas l''temps d''niaiser');
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Wilson', 'Lambert', '100 rue de Dieu', '92200', 'non renseign�', 'Neuilly-sur-Seine', '06xxxxxxxx', 'Bobo des �les', 'wilson.lambert@vetopupuce.com', 'pas l''temps d''niaiser');
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Garcia', 'Jos�', '65 rue de la v�rit�', 'non renseign�', '75001', 'Paris', '06xxxxxxxx', 'Mafassur', 'garcia.jos�@vetopupuce.com', 'pas l''temps d''niaiser');
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Luchini', 'Fabrice', '73 rue de Beaumarchais', 'non renseign�', '75016', 'Paris', '06xxxxxxxx', 'Mafassur', 'luchini.fabrice@vetopupuce.com', 'pas l''temps d''niaiser');
INSERT INTO Clients (Client, PrenomClient, Adresse1, Adresse2, CodePostal, Ville, NumTel, Assurance, Email, Remarque) VALUES('Baye', 'Nathalie', '33 rue de V�nus', 'non renseign�', '27150', 'Mainneville', '06xxxxxxxx', 'Mafassur', 'baye.nathalie@vetopupuce.com', 'pas l''temps d''niaiser');
set identity_insert Clients off;




insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Pastaga', 'M', 'Noir', 'Rotweiller', 'Chien', '1000000001', 'Castr�');
insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Rox', 'M', 'Blanc', 'Labrador', 'Chien', '1000000002', 'Rien � signaler');
insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Nasty', 'F', 'Noir', 'Berger Allemand', 'Chien', '1000000003', 'St�rilis�e');
insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Nanou', 'F', 'Rousse', 'Angora', 'Chat', '1000000004', 'Rien � signaler');
insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Fox', 'M', 'Blanc-Noir', 'Caniche', 'Chien', '1000000005', 'Rien � signaler');
insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Tina', 'F', 'Tigr�e', 'Rotweiller', 'Chien', '1000000006', 'Rien � signaler');
insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Monster', 'M', 'Blanc', 'Dobberman', 'Chien', '1000000007', 'Castr�');
insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Walter', 'H', 'Noir', 'Siamois', 'Chat', '1000000008', 'Rien � signaler');
insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Uber', 'M', 'Blanc', 'Labrador', 'Chien', '1000000009', 'Castr�');
insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Deby', 'F', 'Blanc', 'Rotweiller', 'Chien', '1000000010', 'Rien � signaler');
insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Narco', 'M', 'Noir', 'Colais', 'Chien', '1000000011', 'Castr�');
insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Nara', 'F', 'Noir', 'Sacr� de Birmanie', 'Chat', '1000000012', 'St�rilis�e');
insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Bachus', 'M', 'Roux', 'Labrador', 'Chien', '1000000013', 'Castr�');
insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Caden', 'M', 'Noir', 'Rotweiller', 'Chien', '1000000014', 'Castr�');
insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Fafouille', 'M', 'Noir', 'Colais', 'Chien', '1000000015', 'Castr�');
insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Eden', 'M', 'Blanc-Noir', 'Siamois', 'Chat', '1000000016', 'Rien � signaler');
insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Gabouche', 'M', 'Noir', 'Caniche', 'Chien', '1000000017', 'Castr�');
insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Nesquick', 'H', 'Tigr�e', 'Berger Allemand', 'Chien', '1000000018', 'Rien � signaler');
insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Icar', 'M', 'Blanc', 'Rotweiller', 'Chien', '1000000019', 'Castr�');
insert into Animaux (NomAnimal, Sexe, Couleur, Race, Espece, Tatouage, Antecedents) values('Lacy', 'F', 'Noir', 'Pitbull', 'Chien', '1000000020', 'Rien � signaler');







