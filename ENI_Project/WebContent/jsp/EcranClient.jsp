<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Ecran Client</title>
<style>
	body{
    	border-style: solid;
	}


</style>
</head>

<body>

	<hr/>
    <div class="container">
        <button type="button" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-fast-backward"></span> Premier
        </button>
      	<button type="button" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-menu-left"></span> Précédent
        </button>
        <button type="button" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-menu-right"></span> Suivant
        </button>
      	<button type="button" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-fast-forward"></span> Dernier
        </button>
      	
      	<button type="button" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-plus"> Ajouter</span>
        </button>
      	<button type="button" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-minus"> Supprimer</span>
        </button>
        
        <input type="text" value="Nom du Client">
        <input type="search" value="Rechercher">
    </div>
	<hr/>
    
    <div class="row">
        <div class="col-sm-6">
			<form>
            <div class="form-group">
				<label for="code">CodeClient</label>
            	<input type="bigint" class="form-control">
            </div>
            <div class="form-group">
            	<label for="Nom">Client</label>
                <input type="text" class="form-control">
            </div>
            <div class="form-group">
            	<label for="Prenom">PrenomClient</label>
                <input type="text" class ="form-control">
            </div>
            <div class="form-group">
            	<label for="Adresse">Adresse1</label>
                <input type="text" class="form-control">
            </div>
            <div class="form-group">
            	<label for="Adresse">Adresse2</label>
                <input type="text" class="form-control">
            </div>
            <div class="form-group">
            	<label for="CodePostal">CodePostal</label>
                <input type="text" class="form-control">
            </div>
            <div class="form-group">
            	<label for="Ville">Ville</label>
                <input type="text" class="form-control">
            </div>

            </form>
        </div>
        
        <div class="col-sm-6">
        	<table class="table">
    <thead>
      <tr>
        <th>CodeClient</th>
        <th>Client</th>
        <th>Sexe</th>
        <th>Couleur</th>
        <th>Race</th>
        <th>Espece</th>
        <th>Tatouage</th>
      </tr> 
    </thead>
    
    <tbody>
      <tr>
        <td>1</td>
        <td>Dubosc</td>
        <td>M</td>
        <td>Noir</td>
        <td>Rotweiller</td>
        <td>Chien</td>
        <td>1000000001</td>
      </tr>      
      <tr>
        <td>2</td>
        <td>Boon</td>
        <td>M</td>
        <td>Blanc</td>
        <td>Labrador</td>
        <td>Chien</td>
        <td>1000000002</td>
      </tr>
      <tr>
        <td>4</td>
        <td>Dujardin</td>
        <td>F</td>
        <td>Rousse</td>
        <td>Angora</td>
        <td>Chat</td>
        <td>1000000004</td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    </tbody>
  </table>
        
        <input type="submit" value="ajouter">
        <input type="submit" value="supprimer">
        <input type="submit" value="editer">
    </div>

</body>
</html>