<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="fr">
<head>

  <title>Acceuil</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
  .jumbotron {
      background-color: green;
      color: #fff;
  }
  
   .container-fluid {
      padding: 60px 50px;
  }
  .bg-grey {
      background-color: #f6f6f6;
  }
  .logo {
      font-size: 200px;
  }

  </style>
</head>
<body>
	<div>
  		<ul class="nav nav-tabs">
  		<li class="active"><a href="#PageConnection.jsp" data-toggle="tab">Login</a></li>
    	</ul>
	</div>

<div class="jumbotron text-center">
  <h1>Clinique V�t�rinaire ZooEcolo</h1> 
  <p>Nous sommes sp�cialis�s dans les actes de chirurgies animales et le traitement des diff�rentes maladies avec des produits respectant les normes �cologiques en vigueur !</p> 
</div>



<div class="container">
  <h2>Notre �quipe</h2>  
  <div id="myCarousel" class="carousel slide" data-ride="carousel">

    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
      <li data-target="#myCarousel" data-slide-to="4"></li>
      
    </ol>

    <div class="carousel-inner">
      <div class="item active">
        <img src="../img/directoradmin.jpg" alt="Directeur-Administratif" style="width:100%;">
        <h3>Edmond BOSAPIN</h3>
      </div>

      <div class="item">
        <img src="../img/directortechnique.jpg" alt="Directeur-Technique" style="width:100%;">
        <h3>Benoit DE CAJOU</h3>
      </div>
    
      <div class="item">
        <img src="../img/veto.png" alt="V�t�rinaires" style="width:100%;">
        <h3>M�lanie MALANICH, Sylvan TOURNE et Anne AIMONE</h3>
      </div>
    
      <div class="item">
        <img src="../img/secretaire.JPG" alt="Secr�taire" style="width:100%;">
        <h3>Odette DE JEAU notre secr�taire (Elisabeth ABONDIEU et Marie-Paule HISSON non-visibles)</h3>
      </div>
      
       <div class="item">
        <img src="../img/assistants.jpg" alt="Assistants" style="width:100%;">
      	<h3>Isabelle ELABETE (ainsi que Guillaume SWITAUME et Thibaut MONFILS)</h3>
      </div>
    </div>


    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>







<div class="container-fluid text-center ">
  <h2>SERVICES</h2>
  <h4>Ce que nous vous offrons.</h4>
  <br>
  <div class="row">
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-off"></span>
      <h4>Efficacit�</h4>
      <p>Nos m�thodes sont reconnues par l'OMS.</p>
    </div>
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-heart"></span>
      <h4>Passion</h4>
      <p>Chaque actif de la clinique s'emploie � toujours traiter les animaux avec la passion qu'il m�rite.</p>
    </div>
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-lock"></span>
      <h4>Le "bon-travail"</h4>
      <p>Nous ne prenons pas de cas que nous ne pouvons traiter.</p>
    </div>
  </div>
  <br><br>
  <div class="row">
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-leaf"></span>
      <h4>La r�gle verte</h4>
      <p>Respect de l'environnement et des diff�rentes contraintes �cologiques.</p>
    </div>
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-certificate"></span>
      <h4>Certification de bonne foi</h4>
      <p>Nous nous sommes engag�s � r�pondre de nos actes en cas d'insatisfaction de votre part.</p>
    </div>
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-wrench"></span>
      <h4 style="color:#303030;">Qualit� du travail</h4>
      <p>L'alliance entre la passion du m�tier et le professionalisme de nos salari�s sont les garants de la qualit� de nos prestation.</p>
    </div>
  </div>
</div>


</body>
</html>
