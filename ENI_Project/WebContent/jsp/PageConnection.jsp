<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>


<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link rel="icon" type="image/png" href="https://www.veterinaire-alliance.fr/wp-content/themes/alliance-1.1/images/slider-accueil/04.jpg" />



<style>

 .main {
      position:absolute;
      left:50%;top:0;
      transform:translate(-50%,0%);
      -ms-transform:translate(-50%,0%);

    }

    form {
      margin-top: 50px;
    }

    p {
      text-align: center;
    }


</style>

</head>

<body>
<div class="mainn">
        <div class="main col-xs-10 col-sm-7 col-md-4 col-lg-4">

          <div>
            <ul class="nav nav-tabs">
              <li class="active"><a href="#login" data-toggle="tab">Login</a></li>
            </ul>
          </div>

          <div class="tab-content">

            <div class=" tab-pane fade in active" id="login">

                <form class="form-horizontal">
                  <div class="form-group">
                    <label class="control-label col-sm-3">login</label>
                    <div class="col-sm-9 col-md-9 col-lg-9">
                      <input type="text" class="form-control" id="nom" placeholder="Enter nom">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-3" for="pwd">Password:</label>
                    <div class="col-sm-9 col-md-9 col-lg-9">
                      <input type="password" class="form-control" id="pwd" placeholder="Enter password">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                      <div class="checkbox">
                        <label><input type="checkbox"> Remember me</label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                      <button type="submit" class="btn btn-default">Login In</button>
                    </div>
                  </div>
                </form>
            </div>

       </div>

        </div>
   
      </div>
</body>
</html>