package fr.eni_ecole.clinique.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServlet;
import fr.eni_ecole.clinique.beans.Personnels;

/**
 * Servlet implementation class PersonnelsDAL
 */
public class PersonnelsDAL extends HttpServlet implements Servlet {
	private static final long serialVersionUID = 1L;
	
	private final static String SelectAll = "select Nom, Role, MotPasse from Personnels;";
	private final static String Ajouter = "insert into Personnels(Nom, MotPasse, Role) values (?,?,?);";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	public static Personnels getConnection(Personnels personnels) throws SQLException{
		Connection cnx = null;
		PreparedStatement rqt = null;
		ResultSet rs = null;
		try{
			cnx = ConnectionBase.getConnection();
			rqt = cnx.prepareStatement("select Nom, MotPasse from Personnels where Nom=? and MotPasse=?;");
			rqt.setString(1, personnels.getNom());
			rqt.setString(2, personnels.getMotPasse());
			rs=rqt.executeQuery();
			// SI on trouve au moins 1 r�sultat, on prend le 1er pour mettre � jour les informations de l'animateur utilis� pour la recherche.
			if (rs.next()){
				personnels.setNom(rs.getString("nom"));
				personnels.setMotPasse(rs.getString("motpasse"));
			}
			// ...sinon on renvoie null
			else {
				personnels = null;
			}
			
		}finally{
			if (rs!=null) rs.close();
			if (rqt!=null) rqt.close();
			if (cnx!=null) cnx.close();
		}
		return personnels;
	}

	public static ArrayList<Personnels> Lister() throws SQLException{
		//declarations
		Connection cnx=null;
		Statement rqt=null;
		ResultSet rs=null;
		ArrayList<Personnels> listePersonnels = new ArrayList<Personnels>();
		
		try{
			//se connecter � la base via le pool de connexion
			cnx = ConnectionBase.getConnection();
			//creer mon statement
			rqt = cnx.createStatement();
			//charge mon resultset
			rs = rqt.executeQuery(SelectAll);
			//instancier nom eleve
			
			//recuperation des valeurs et chargement de la liste des eleves
			while(rs.next()){
				Personnels personnel = new Personnels();
				personnel.setNom(rs.getString("Nom"));
				personnel.setRole(rs.getString("Role"));
				personnel.setMotPasse(rs.getString("MotPasse"));
				listePersonnels.add(personnel);
			}
		}catch (Exception e) {
		        System.out.println(e);
		    
		}finally{
			if(rqt!=null) rqt.close();
			if(rs!=null) rs.close();
			if(cnx!=null) cnx.close();
		}
		
		return listePersonnels;
	}
	
	public static void ajouter(Personnels personnels) throws SQLException{
		Connection cnx=null;
		PreparedStatement rqt=null;

		try{
			cnx = ConnectionBase.getConnection();
			rqt=cnx.prepareStatement(Ajouter);
			rqt.setString(1, personnels.getNom());
			rqt.setString(2, personnels.getMotPasse());
			rqt.setString(3, personnels.getRole());
			rqt.executeUpdate();
		}finally{
			if (rqt!=null) rqt.close();
			if (cnx!=null) cnx.close();
		}
	}
}


