package fr.eni_ecole.clinique.dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sun.xml.internal.ws.api.message.Message;

import fr.eni_ecole.clinique.beans.Personnels;

public class ConnectionBDA{

	public static Connection seConnecter() {
	Connection cnx = null;
	ResultSet rs = null;
	PreparedStatement rqt = null;
	String url = "jdbc:sqlserver://localhost:8080/ENI_pro";
	String utilisateur = "root";
	String MotDePasse = "root";

	try{
		System.out.println("Connection � la base de donn�es...");
		cnx = DriverManager.getConnection(url, utilisateur, MotDePasse);
		System.out.println("Connection r�ussie !");
		//ex�cution de la req�te de lecture
		rs = rqt.executeQuery("select Nom, MotPasse, Role from Personnels;");
		System.out.println("Requ�te\"select Nom, MotPasse, Role;\" effectu�e !");
		
		//Recuperer donn�es de r�sultat
		while(rs.next()){
		
			
			String login = rs.getString("Nom");
			String MotPasse = rs.getString("MotPasse");
			String role = rs.getString("Role");
			
			return (login + MotPasse + role);
				
		//formatage des donn�es pour afficher la jsp
		System.out.println("Message retourn�e par la requ�te.");
		}
	}catch (SQLException e) {
		System.out.println("Erreur de la connection :" + e.getMessage());
	}finally {
		System.out.println("Fermeture de l'objet ResultSet.");
		if(rs != null){
			try{
				rs.close();
				}catch (SQLException ignore){
			}
		}
		System.out.println("Fermeture de l'objet Statement");
		if (rqt != null){
			try{
				rqt.close();
			}catch (SQLException ignore){					
		 }
		}
		System.out.println("Fermeture de l'objet Connection.");
		if (cnx != null){
			try{
				cnx.close();
	}catch (SQLException ignore){
			}
	}
	
		return (cnx);
	}
}	
}