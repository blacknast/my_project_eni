package fr.eni_ecole.clinique.dal;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConnectionBase {

	public static Connection getConnection() throws SQLException{
		
		Connection cnx=null;
		InitialContext init = null;
		DataSource ds = null;
		
		//obtenir une r�f�rence sur le contexte initial
		try{
		init = new InitialContext();
		}catch(NamingException e){
			throw new SQLException ("Erreur d'acc�s au contexte initial.");
		}
		//recherche un pool de connexion
		try{
			ds = (DataSource)init.lookup("java:comp/env/jdbc/ENI-pro");
		}catch(NamingException e){
			throw new SQLException ("Objet introuvable.");
		}
		//obtenir une connexion
		try{
			cnx = ds.getConnection();
			return cnx;
		}catch(SQLException e){
			throw new SQLException ("Impossible d'obtenir une connexion");
		}
	}
	
}
