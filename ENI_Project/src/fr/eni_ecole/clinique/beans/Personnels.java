package fr.eni_ecole.clinique.beans;

public class Personnels {

	private long CodePers;
	private String Nom;
	private String MotPasse;
	private String Role;
	private boolean Archive;
	
	public long getCodePers() {
		return CodePers;
	}
	public void setCodePers(long codePers) {
		CodePers = codePers;
	}
	public String getNom() {
		return Nom;
	}
	public void setNom(String nom) {
		this.Nom = nom;
	}
	public String getMotPasse() {
		return MotPasse;
	}
	public void setMotPasse(String motPasse) {
		this.MotPasse = motPasse;
	}
	public String getRole() {
		return Role;
	}
	public void setRole(String role) {
		this.Role = role;
	}
	public boolean isArchive() {
		return Archive;
	}
	public void setArchive(boolean archive) {
		this.Archive = archive;
	}
	
	public Personnels() {
		super();
	}
	
	public Personnels(long codePers, String nom, String motPasse, String role, boolean archive) {
		this();
		setNom(nom);
		setMotPasse(motPasse);
		setRole(role);
		setArchive(archive);
		
	}
	
	
		
}
