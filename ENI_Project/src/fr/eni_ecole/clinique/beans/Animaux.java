package fr.eni_ecole.clinique.beans;

public class Animaux extends Clients{
	
	private long CodeAnimal;
	private String NomAnimal;
	private String Sexe;
	private String Couleur;
	private String Race;
	private String Espece;
	private long CodeClient;
	private String Tatouage;
	private String Antecedents;
	private boolean Archive;
	public long getCodeAnimal() {
		return CodeAnimal;
	}
	public void setCodeAnimal(long codeAnimal) {
		CodeAnimal = codeAnimal;
	}
	public String getNomAnimal() {
		return NomAnimal;
	}
	public void setNomAnimal(String nomAnimal) {
		NomAnimal = nomAnimal;
	}
	public String getSexe() {
		return Sexe;
	}
	public void setSexe(String sexe) {
		Sexe = sexe;
	}
	public String getCouleur() {
		return Couleur;
	}
	public void setCouleur(String couleur) {
		Couleur = couleur;
	}
	public String getRace() {
		return Race;
	}
	public void setRace(String race) {
		Race = race;
	}
	public String getEspece() {
		return Espece;
	}
	public void setEspece(String espece) {
		Espece = espece;
	}
	public long getCodeClient() {
		return CodeClient;
	}
	public void setCodeClient(long codeClient) {
		CodeClient = codeClient;
	}
	public String getTatouage() {
		return Tatouage;
	}
	public void setTatouage(String tatouage) {
		Tatouage = tatouage;
	}
	public String getAntecedents() {
		return Antecedents;
	}
	public void setAntecedents(String antecedents) {
		Antecedents = antecedents;
	}
	public boolean isArchive() {
		return Archive;
	}
	public void setArchive(boolean archive) {
		Archive = archive;
	}

	
	
	public Animaux(long codeClient, String client, String prenomClient, String adresse1, String adresse2,
			String codePostal, String ville, String numTel, String assurance, String email, String remarque,
			boolean archive) {
		super(codeClient, client, prenomClient, adresse1, adresse2, codePostal, ville, numTel, assurance, email, remarque,
				archive);
		// TODO Auto-generated constructor stub
	}

	
	
	
	
	
}
