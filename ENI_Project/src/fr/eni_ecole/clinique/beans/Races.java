package fr.eni_ecole.clinique.beans;

public class Races {

	private String Race;
	private String Espece;
	public String getRace() {
		return Race;
	}
	public void setRace(String race) {
		Race = race;
	}
	public String getEspece() {
		return Espece;
	}
	public void setEspece(String espece) {
		Espece = espece;
	}
	public Races(String race, String espece) {
		super();
		Race = race;
		Espece = espece;
	}
	
	
	
}
