package fr.eni_ecole.clinique.beans;

public class Agendas {
	private long CodeVeto;
	private String DateRdv;
	private long CodeAnimal;
	public long getCodeVeto() {
		return CodeVeto;
	}
	
	public void setCodeVeto(long codeVeto) {
		CodeVeto = codeVeto;
	}
	public String getDateRdv() {
		return DateRdv;
	}
	public void setDateRdv(String dateRdv) {
		DateRdv = dateRdv;
	}
	public long getCodeAnimal() {
		return CodeAnimal;
	}
	public void setCodeAnimal(long codeAnimal) {
		CodeAnimal = codeAnimal;
	}

	public Agendas(long codeVeto, String dateRdv, long codeAnimal) {
		super();
		CodeVeto = codeVeto;
		DateRdv = dateRdv;
		CodeAnimal = codeAnimal;
	}
	
	
	
	
	

}
