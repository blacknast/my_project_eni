package fr.eni_ecole.clinique.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni_ecole.clinique.beans.Personnels;
import fr.eni_ecole.clinique.dal.PersonnelsDAL;


/**
 * Servlet implementation class ValiderAccesAnimateur
 */
public class ValiderAccesBase extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		RequestDispatcher dispatcher;
		try {
			ArrayList<Personnels> listePersonnels = PersonnelsDAL.Lister();
			request.getSession().setAttribute("listePersonnels", listePersonnels);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		Personnels personnelsConnect = new Personnels();

		// R�cup�ration des informations saisies dans le formulaire
		String nom = request.getParameter("nom");
		String motdepasse = request.getParameter("motdepasse");
		String role = request.getParameter("role");

		// Controle des informations :
		// si tous les champs ne sont pas renseign�s, revenir sur la page du formulaire
		

		personnelsConnect.setNom(nom);
		personnelsConnect.setMotPasse(motdepasse);
		personnelsConnect.setRole(role);
		

		
		
		try {
			
			personnelsConnect = PersonnelsDAL.getConnection(personnelsConnect);
			
			if(personnelsConnect != null){
				dispatcher = getServletContext().getRequestDispatcher("/pages/employes.jsp");
				dispatcher.forward(request, response);
			}else{
				System.err.println("Error");
			}
			
			
			}
        catch (SQLException sqle) 
		{
        	sqle.printStackTrace();
			// Placer l'objet repr�sentant l'exception dans le contexte de requete
			request.setAttribute("erreur", sqle);
			// Passer la main � la page de pr�sentation des erreurs
			dispatcher = getServletContext().getRequestDispatcher("/erreur/erreur.jsp");
			dispatcher.forward(request, response);
			return;
		}		
	}
}
