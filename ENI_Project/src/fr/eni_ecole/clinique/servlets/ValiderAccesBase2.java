package fr.eni_ecole.clinique.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni_ecole.clinique.beans.Clients;


/**
 * Servlet implementation class ValiderAccesAnimateur
 */
public class ValiderAccesBase2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		RequestDispatcher dispatcher;
		try {
			ArrayList<Clients> listeClients = ClientDAL.Lister();
			request.getSession().setAttribute("listeClients", listeClients);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		String renvoie=null;
		Clients ClientConnect = new Clients();
		renvoie="/WebContent/jsp/GestionPersonnel.jsp";

		// R�cup�ration des informations saisies dans le formulaire
		String nomclient = request.getParameter("nomclient");
		

		// Controle des informations :
		// si tous les champs ne sont pas renseign�s, revenir sur la page du formulaire
		

		ClientConnect.setClient(nomclient);
		

		
		try {
			
			ClientConnect = ClientDAL.getConnection(ClientConnect);
			
			if(ClientConnect != null){
				dispatcher = getServletContext().getRequestDispatcher(renvoie);
				dispatcher.forward(request, response);
			}else{
				System.err.println("Error");
			}
			
			
			}
        catch (SQLException sqle) 
		{
        	sqle.printStackTrace();
			// Placer l'objet repr�sentant l'exception dans le contexte de requete
			request.setAttribute("erreur", sqle);
			// Passer la main � la page de pr�sentation des erreurs
			dispatcher = getServletContext().getRequestDispatcher("");
			dispatcher.forward(request, response);
			return;
		}		
			
		
		
	}
}
