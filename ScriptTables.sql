USE [master]/* Copyright JKT*/
DROP DATABASE ENI_pro;/* Copyright JKT*/
CREATE DATABASE ENI_pro;/* executer uniquement cette ligne si vous n'avez pas d�j� la base */
USE [ENI_pro]/* Copyright JKT*/


/******************************************************************** CREATION DE LA TABLE Clients **/
CREATE TABLE Clients (/* Copyright JKT*/
[CodeClient] BIGINT IDENTITY(1,1) PRIMARY KEY,/* Copyright JKT*/
[Client] VARCHAR(20) NOT NULL,/* Copyright JKT*/
[PrenomClient] VARCHAR(20),/* Copyright JKT*/
[Adresse1] VARCHAR(30),/* Copyright JKT*/
[Adresse2] VARCHAR(30),/* Copyright JKT*/
[CodePostal] VARCHAR(6),/* Copyright JKT*/
[Ville] VARCHAR(25),/* Copyright JKT*/
[NumTel] VARCHAR(15),/* Copyright JKT*/
[Assurance] VARCHAR(30),/* Copyright JKT*/
[Email] VARCHAR(30),/* Copyright JKT*/
[Remarque] TEXT,/* Copyright JKT*/
[Archive] BIT DEFAULT 0/* Copyright JKT*/
)
/******************************************************************** CREATION DE LA TABLE Races **/
CREATE TABLE Races (
[Race] VARCHAR(20),/* Copyright JKT*/
[Espece] VARCHAR(20),/* Copyright JKT*/
CONSTRAINT PK_race_espece PRIMARY KEY (Race,Espece)/* Copyright JKT*/
)
/******************************************************************** CREATION DE LA TABLE Animaux **/
CREATE TABLE Animaux (
[CodeAnimal] BIGINT IDENTITY(1,1) PRIMARY KEY,/* Copyright JKT*/
[NomAnimal] VARCHAR(30),/* Copyright JKT*/
[Sexe] VARCHAR(1),/* Copyright JKT*/
[Couleur] VARCHAR(20),/* Copyright JKT*/
[Race] VARCHAR(20),/* Copyright JKT*/
[Espece] VARCHAR(20),/* Copyright JKT*/
[CodeClient] BIGINT,/* Copyright JKT*/
[Tatouage] VARCHAR(10) UNIQUE,/* Copyright JKT*/
[Antecedents] TEXT,/* Copyright JKT*/
[Archive] BIT DEFAULT 0,/* Copyright JKT*/
CONSTRAINT FK_code_client FOREIGN KEY (CodeClient) REFERENCES Clients(CodeClient),/* Copyright JKT*/
CONSTRAINT FK_race_espece FOREIGN KEY (Race,Espece) REFERENCES Races(Race,Espece),/* Copyright JKT*/
CONSTRAINT CHK_sexe_m_f_h CHECK (Sexe='M'OR Sexe='F'OR Sexe='H'OR Sexe='m'OR Sexe='f'OR Sexe='h')/* Copyright JKT*/
)/* Copyright JKT*/
/******************************************************************** CREATION DE LA TABLE Personnels **/
CREATE TABLE Personnels (/* Copyright JKT*/
[CodePers] BIGINT IDENTITY(1,1) PRIMARY KEY,/* Copyright JKT*//* Copyright JKT*/
[Nom] VARCHAR(30),/* Copyright JKT*//* Copyright JKT*/
[MotPasse] VARCHAR(10),/* Copyright JKT*//* Copyright JKT*/
[Role] VARCHAR(3),/* Copyright JKT*/
[Archive] BIT DEFAULT 0/* Copyright JKT*/
)
/******************************************************************** CREATION DE LA TABLE Agendas **/
CREATE TABLE Agendas (/* Copyright JKT*/
[CodeVeto] BIGINT,/* Copyright JKT*/
[DateRdv] DATETIME,/* Copyright JKT*/
[CodeAnimal] BIGINT,/* Copyright JKT*/
CONSTRAINT PK_codeveto_daterdv_codeanimal PRIMARY KEY (CodeVeto,DateRdv,CodeAnimal),/* Copyright JKT*/
CONSTRAINT FK_codeveto_codepers FOREIGN KEY (CodeVeto) REFERENCES Personnels(CodePers),/* Copyright JKT*/
CONSTRAINT FK_codeanimal_animal FOREIGN KEY (CodeAnimal) REFERENCES Animaux(CodeAnimal),/* Copyright JKT*/
CONSTRAINT CHK_quartdheure CHECK (DATEPART(minute,DateRdv)='00'OR DATEPART(minute,DateRdv)='15'OR DATEPART(minute,DateRdv)='30'OR DATEPART(minute,DateRdv)='45'),/* Copyright JKT*/
CONSTRAINT CHK_zero_second_ms CHECK (DATEPART(second,DateRdv)='00'AND DATEPART(millisecond,DateRdv)='000'),/* Copyright JKT*/
CONSTRAINT UNQ_unrdvparveto UNIQUE (CodeVeto,DateRdv)/* Copyright JKT*/
)/* Copyright JKT*/


select * from Personnels;
select * from Races;
select * from Clients;
select * from Animaux;
select * from Agendas;
